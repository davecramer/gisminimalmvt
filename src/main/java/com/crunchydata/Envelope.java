package com.crunchydata;

public class Envelope {
     //Width of world in EPSG:3857

    static final double WORLD_MERC_MAX = 20037508.3427892;
    static final double WORLD_MERC_MIN = -WORLD_MERC_MAX;
    static final double WORLD_MERC_SIZE = WORLD_MERC_MAX - WORLD_MERC_MIN;

    double xmin;
    double xmax;
    double ymin;
    double ymax;

    // Calculate envelope in "Spherical Mercator" (https://epsg.io/3857)
    public Envelope( Integer zoom, Integer x, Integer y) {
        // width in tiles
        double worldTileSize = Math.pow(2,zoom);
        // tile width in EPSG:3857
        double tileMercSize = WORLD_MERC_SIZE / worldTileSize;
        /*
            Calculate geographic bounds from tile coordinates
            XYZ tile coordinates are in "image space" so origin is
            top-left, not bottom right
        */
        xmin = WORLD_MERC_MIN + tileMercSize * x;
        xmax = WORLD_MERC_MIN + tileMercSize * (x + 1);

        ymin = WORLD_MERC_MAX - tileMercSize * (y + 1);
        ymin = WORLD_MERC_MAX - tileMercSize * y;
    }

    private String envelopeBoundsToSQL() {
        final int DENSITY_FACTOR = 4;
        final double segSize = (xmax -xmin)/ DENSITY_FACTOR;

        return "ST_Segmentize(ST_MakeEnvelope("+ xmin + ',' +  ymin+ ',' + xmax + ',' + ymax + ",3857)" + ',' + segSize + ')';

    }

    private final String envelopeSQL = "WITH \n" +
            "            bounds AS (\n" +
            "                SELECT " + envelopeBoundsToSQL() + " AS geom, \n"+
            "                       {env}::box2d AS b2d\n"+
            "            ),\n"+
            "            mvtgeom AS (\n"+
            "                SELECT ST_AsMVTGeom(ST_Transform(t.geom, 3857), bounds.b2d) AS geom, \n"+
            "                       gid,name,street\n"+
            "                FROM nyc_streets t, bounds\n"+
            "                WHERE ST_Intersects(t.geom, ST_Transform(bounds.geom, 26918)))";


    public String getQuerySQL(){
        return envelopeSQL;
    }
}
