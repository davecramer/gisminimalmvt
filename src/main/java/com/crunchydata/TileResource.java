package com.crunchydata;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public class TileResource {

    @Inject
    io.reactiverse.axle.pgclient.PgPool client;

    private static final MediaType MEDIA_TYPE = new MediaType("application", "vnd.mapbox-vector-tile");

    @GET
    @Produces(MEDIA_TYPE)
    @Path("/{zoom}/{x}/{y}")
    public Response tile(@PathParam("zoom") Integer zoom, @PathParam("x") Integer x, @PathParam("y") Integer y, @QueryParam("format") String format) {
        Envelope envelope = new Envelope(zoom, x, y);
        client.query(envelope.getQuerySQL())
        return Response.ok.build();
    }
}