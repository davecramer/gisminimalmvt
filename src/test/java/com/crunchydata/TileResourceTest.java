package com.crunchydata;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class TileResourceTest {

    @Test
    public void testHelloEndpoint() {
        given()
          .when().get("/100/1/2?format=pdf")
          .then()
             .statusCode(200)
             .body(is("zoom: 100 x: 1 y: 2 format: pdf"));
    }

}